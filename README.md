# Javen 微信公众号极速开发
weixin_guide 是基于 JFinal 的微信公众号极速开发 SDK，只需浏览 Demo 代码即可进行极速开发，自 JFinal Weixin 1.2 版本开始已添加对多公众号支持。


## 1、运行截图
`详见请见`：[项目运行截图](http://git.oschina.net/javen205/weixin_guide/wikis/%E8%BF%90%E8%A1%8C%E6%95%88%E6%9E%9C%E5%9B%BE)

## 2、WIKI持续更新中
`详见请见`：[WIKI](http://git.oschina.net/javen205/weixin_guide/wikis/home)


[详细的图文教程-项目导入IDE并启动运行、如何成为开发者模式、如何实现消息交互、如何自定义菜单、如何授权获取用户信息 等](http://www.jianshu.com/p/a172a1b69fdd)

`欢迎更多同学来帮助完善！`

## 3、版本更新记录
`详见请见`：[更新说明](http://git.oschina.net/javen205/weixin_guide/wikis/%E7%89%88%E6%9C%AC%E6%9B%B4%E6%96%B0%E8%AE%B0%E5%BD%95)

如发现`bug`或者有更好的建议以及意见请发`Issues` 帮助项目迭代更新

## 4、JFinal weixin的使用参考
`详情请见`：[JFinal weixin中的配置以及接口的使用](http://git.oschina.net/jfinal/jfinal-weixin/wikis/home)

## 5、非Maven用户得到所有依赖 jar 包两种方法
- 将项目导入eclipse jee中，使用 export 功能导出 war包，其中的 WEB-INF/lib 下面会自动生成 jar 包
- 让使用 maven 的朋友使用 mvn package 打出 war包，其中的 WEB-INF/lib 下面会自动生成 jar 包
- 以上两种方法注意要先将pom.xml中的导出类型设置为 war，添加 <packaging>war</packaging> 内容进去即可
- 依赖jackson或fastjson



## 6、更多支持
- 交流群：148540125(备注项目名称、进群发广告T)
- Email：javen205@126.com

## 7、鸣谢

JFinal
Jfinal-weixin



